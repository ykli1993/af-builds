#! /bin/bash

AFL_Binary=afl-fuzz
Target=./readpng-cfast
Start_of_Tests=1
Num_of_Tests=4
Tmux_Session=readpng-normal

tmux has-session -t $Tmux_Session 2> /dev/null;

if [[ $? != 0 ]]; then
    tmux new-session -d -s $Tmux_Session;
    for i in $(seq 1 $Num_of_Tests); do
        let output_dir_num="$i + $Start_of_Tests - 1";
        tmux new-window -t "$Tmux_Session:$i" -n "$Target-$i";
        if (( $i % 2 == 0 )); then
            let output_dir_num="$i + $Start_of_Tests - 2";
            tmux send-keys  -t "$Tmux_Session:$i" "$AFL_Binary -i seeds/ -o out-$output_dir_num/ -t 50 -m none -S slave -- $Target @@"   C-m ;
        else
            tmux send-keys  -t "$Tmux_Session:$i" "$AFL_Binary -i seeds/ -o out-$output_dir_num/ -t 50 -m none -M master -- $Target @@"   C-m ;
        fi
    done;
else
    printf "tmux session $Tmux_Session exists.\n=========\n`tmux ls 2>/dev/null`."
fi;
